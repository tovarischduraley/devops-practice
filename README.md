# Docker/Kubernetes practice

### Выполненные шаги:

- Были описаны server.py и client.py 
- Были добавлены Dockerfile'ы для сервера и клиента
- Образ сервера был загружен в [dockerhub](https://hub.docker.com/r/stvldmr/server)
- Был описан deployment манифест запускающий контейнер из занруженного образа
- Манифест был установлен в кластер
- Был обеспечен доступ к приложению с помощью ```kubectl port-forward --address 0.0.0.0 deployment/web 8080:8000```

### Результаты:
```
kubectl describe deployment web

Name:                   web
Namespace:              default
CreationTimestamp:      Sun, 14 May 2023 19:17:51 +0300
Labels:                 app=web
Annotations:            deployment.kubernetes.io/revision: 1
Selector:               app=web
Replicas:               2 desired | 2 updated | 2 total | 2 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=web
  Containers:
   web:
    Image:        stvldmr/server:1.0.0
    Port:         8000/TCP
    Host Port:    0/TCP
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   web-75fd5b775f (2/2 replicas created)
Events:
  Type    Reason             Age    From                   Message
  ----    ------             ----   ----                   -------
  Normal  ScalingReplicaSet  4m47s  deployment-controller  Scaled up replica set web-75fd5b775f to 2
  ```
![Screenshot1.png](screenshots%2FScreenshot1.png)